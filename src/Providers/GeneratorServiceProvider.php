<?php

namespace Weltkind\Modules\Providers;

use Illuminate\Support\ServiceProvider;

class GeneratorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        $generators = [
            'command.make.module'            => \Weltkind\Modules\Console\Generators\MakeModuleCommand::class,
            'command.make.module.controller' => \Weltkind\Modules\Console\Generators\MakeControllerCommand::class,
            'command.make.module.middleware' => \Weltkind\Modules\Console\Generators\MakeMiddlewareCommand::class,
            'command.make.module.migration'  => \Weltkind\Modules\Console\Generators\MakeMigrationCommand::class,
            'command.make.module.model'      => \Weltkind\Modules\Console\Generators\MakeModelCommand::class,
            'command.make.module.policy'     => \Weltkind\Modules\Console\Generators\MakePolicyCommand::class,
            'command.make.module.provider'   => \Weltkind\Modules\Console\Generators\MakeProviderCommand::class,
            'command.make.module.request'    => \Weltkind\Modules\Console\Generators\MakeRequestCommand::class,
            'command.make.module.seeder'     => \Weltkind\Modules\Console\Generators\MakeSeederCommand::class,
            'command.make.module.test'       => \Weltkind\Modules\Console\Generators\MakeTestCommand::class,
        ];

        foreach ($generators as $slug => $class) {
            $this->app->singleton($slug, function ($app) use ($slug, $class) {
                return $app[$class];
            });

            $this->commands($slug);
        }
    }
}
