<?php

namespace Weltkind\Modules\Exceptions;

class ModuleNotFoundException extends \Exception
{
    /**
     * ModuleNotFoundException constructor.
     *
     * @param $slug
     * @return void
     */
    public function __construct($slug)
    {
        parent::__construct('Module with slug name [' . $slug . '] not found');
    }
}
